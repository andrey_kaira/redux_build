import 'package:reduxtask/app_routes.dart';
import 'package:reduxtask/state/app_state.dart';
import 'package:redux/redux.dart';
import 'navigation_reducer.dart';

AppState appStateReducer(AppState state, action) {
  return AppState(
     route: navigationReducer(state.route, action),
     isLoading: loadingReducer(state.isLoading, action),
  );
}



final loadingReducer = combineReducers<bool>([
]);
