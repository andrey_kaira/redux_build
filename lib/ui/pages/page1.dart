import 'package:flutter/material.dart';
import 'package:reduxtask/widgets/main_drawer.dart';

class Page1 extends StatefulWidget {
  @override
  _Page1State createState() => _Page1State();
}

class _Page1State extends State<Page1> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: MainDrawer(),
      appBar: AppBar(
        title: Text('Page 1'),
      ),
      body: Container(
        color: Colors.green,
      ),
    );
  }
}
