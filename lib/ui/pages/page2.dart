import 'package:flutter/material.dart';
import 'package:reduxtask/widgets/main_drawer.dart';

class Page2 extends StatefulWidget {
  @override
  _Page2State createState() => _Page2State();
}

class _Page2State extends State<Page2> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: MainDrawer(),
      appBar: AppBar(
        title: Text('Page 2'),
      ),
      body: Container(
        color: Colors.deepPurple,
      ),
    );
  }
}
