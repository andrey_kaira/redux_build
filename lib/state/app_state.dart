import 'package:flutter/cupertino.dart';
import 'package:reduxtask/app_routes.dart';

@immutable
class AppState {
  final List<String> route;
  final bool isLoading;
  AppState({
    this.isLoading = false,
    this.route = const [AppRoutes.home],
  });

  factory AppState.loading() => AppState(isLoading: true);
}
