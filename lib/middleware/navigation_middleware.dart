import 'package:reduxtask/actions/actions.dart';
import 'package:reduxtask/state/app_state.dart';
import 'package:reducer/reducer.dart';
import 'package:redux/redux.dart';

import '../main.dart';


List<Middleware<AppState>> createNavigationMiddleware() {
  return [
    TypedMiddleware<AppState, NavigateReplaceAction>(_navigateReplace),
    TypedMiddleware<AppState, NavigatePushAction>(_navigate),
  ];
}

_navigateReplace(Store<AppState> store, action, NextDispatcher next) {
  final routeName = (action as NavigateReplaceAction).routeName;
  if (store.state.route.last != routeName) {
    navigatorKey.currentState.pushReplacementNamed(routeName);
  }
  next(action); //This need to be after name checks
}

_navigate(Store<AppState> store, action, NextDispatcher next) {
  final routeName = (action as NavigatePushAction).routeName;
  print('Test');
  if (store.state.route.last != routeName) {
    print('Test2');
    navigatorKey.currentState.pushNamed(routeName);
  }
  next(action); //This need to be after name checks
}