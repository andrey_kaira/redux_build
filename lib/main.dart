import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:flutter_redux_navigation/flutter_redux_navigation.dart';
import 'package:reducer/reducer.dart';
import 'package:redux/redux.dart';
import 'package:redux_epics/redux_epics.dart';
import 'package:reduxtask/reducer/app_state_reducer.dart';
import 'package:reduxtask/route_observer/route_observer.dart';
import 'package:reduxtask/state/app_state.dart';
import 'package:reduxtask/ui/pages/page1.dart';
import 'package:reduxtask/ui/pages/page2.dart';

import 'app_routes.dart';
import 'middleware/navigation_middleware.dart';

final GlobalKey<NavigatorState> navigatorKey = new GlobalKey<NavigatorState>();

void main() {
  return runApp(Application());
}

MaterialPageRoute _getRoute(RouteSettings settings) {
  switch (settings.name) {
    case AppRoutes.home:
      return MainRoute(Page1(), settings: settings);
    case AppRoutes.page2:
      return MainRoute(Page2(), settings: settings);
    default:
      return MainRoute(Page1(), settings: settings);
  }
}

class Application extends StatelessWidget {
  final store = new Store<AppState>(
    appStateReducer,
    initialState: AppState.loading(),
    middleware:createNavigationMiddleware(),
  );

  @override
  Widget build(BuildContext context) {
    return StoreProvider(
      store: store,
      child: MaterialApp(
        navigatorKey: navigatorKey,
        navigatorObservers: [routeObserver],
        theme: ThemeData.dark(),
        onGenerateRoute: (RouteSettings settings) => _getRoute(settings),
        home: Page1(),
      ),
    );
  }
}

class MainRoute<T> extends MaterialPageRoute<T> {
  MainRoute(Widget widget, {RouteSettings settings})
      : super(
            builder: (_) => RouteAwareWidget(child: widget),
            settings: settings);

  @override
  Widget buildTransitions(BuildContext context, Animation<double> animation,
      Animation<double> secondaryAnimation, Widget child) {
    return child;
  }
}

class FabRoute<T> extends MaterialPageRoute<T> {
  FabRoute(Widget widget, {RouteSettings settings})
      : super(
            builder: (_) => RouteAwareWidget(child: widget),
            settings: settings);

  @override
  Widget buildTransitions(BuildContext context, Animation<double> animation,
      Animation<double> secondaryAnimation, Widget child) {
    return child;
  }
}

Stream<dynamic> jokerEpic(
  Stream<dynamic> actions,
  EpicStore<AppState> store,
) async* {
  // Use the `await for` keyword to listen to the stream inside an async*
  // function!
  //await for (var action in actions) {
  //  // Then check to see if we've received an increment action
  //  if (action == Actions.increment) {
  //    // If so, emit a decrement action to the Stream using the `yield` keyword!
  //    // This decrement action will be automatically dispatched.
  //    yield Actions.decrement;
  //  }
  //}
}
