import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:reduxtask/actions/actions.dart';
import 'package:reduxtask/state/app_state.dart';
import 'package:reduxtask/ui/pages/page1.dart';
import 'package:reduxtask/ui/pages/page2.dart';

import '../app_routes.dart';

class MainDrawer extends StatefulWidget {
  @override
  _MainDrawerState createState() => _MainDrawerState();
}

class _MainDrawerState extends State<MainDrawer> {
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: Column(
        children: <Widget>[
          AppBar(
            automaticallyImplyLeading: false,
          ),
          ListTile(
              title: Text('Page 1'),
              onTap: () {
                StoreProvider.of<AppState>(context).dispatch(
                  NavigatePushAction(AppRoutes.home),
                );
              }),
          ListTile(
            title: Text('Page 2'),
            onTap: () {
              StoreProvider.of<AppState>(context).dispatch(
                NavigatePushAction(AppRoutes.page2),
              );
            },
          ),
        ],
      ),
    );
  }
}
