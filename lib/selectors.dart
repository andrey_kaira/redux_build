import 'package:reduxtask/state/app_state.dart';

List<String> currentRoute(AppState state) => state.route;